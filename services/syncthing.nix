{ config, lib, pkgs, ... }:

{
  services.syncthing = {
    enable = true;
    dataDir = "/data/syncthing";
    openDefaultPorts = true;
  };

  # syncthing over quic
  networking.firewall.allowedUDPPorts = [ 22000 ];
}
