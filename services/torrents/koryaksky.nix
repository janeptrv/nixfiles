{ config, lib, pkgs, profiles, modules, evalConfig, ... }:

{
  fileSystems."/data/nfs" = {
    device = "172.23.1.2:/data/torrents";
    fsType = "nfs";
    options = [ "x-systemd.automount" "noauto" ];
  };

  containers.torrents = {
    privateNetwork = true;
    hostAddress = "192.168.100.1";
    localAddress = "192.168.100.2";
    autoStart = true;
    bindMounts = {
      "/data" = {
        hostPath = "/data/nfs";
        isReadOnly = false;
      };
      "/persist" = {
        hostPath = "/persist/container/torrents";
        isReadOnly = false;
      };
    };
    path = (evalConfig {hosts = {}; groups = {};} ({ config, lib, pkgs, profiles, modules, ... }: {
      boot.isContainer = true;
      networking.useDHCP = false;
      users.users.root.hashedPassword = "";

      imports = [
        profiles.nopersist
      ];

      nixpkgs.config.allowUnfree = true;
      networking.firewall.enable = false;
      networking.interfaces.eth0.ipv4.routes = [{
        address = "193.27.14.82";
        prefixLength = 32;
        via = "192.168.100.1";
      }];
      # this is so i don't accidentally get DMCAs lol
      networking.wireguard.enable = true;
      networking.wireguard.interfaces.mullvad = {
        allowedIPsAsRoutes = true;
        generatePrivateKeyFile = true;
        privateKeyFile = "/persist/wireguard/mullvad-key";
        ips = ["10.64.155.50/32" "fc00:bbbb:bbbb:bb01::1:9b31/128"];
        peers = [{
          allowedIPs = [ "0.0.0.0/0" "::0/0" ];
          publicKey = "gB+j+wTsZ8dZq2TbUQovV6zwmM9O2SMneGQR6NHxOQQ=";
          endpoint = "193.27.14.82:51820";
        }];
      };

      services.deluge = {
        enable = true;
        dataDir = "/persist/deluge";
        declarative = true;
        authFile = "/persist/deluge/auth";
        extraPackages = [ pkgs.p7zip pkgs.unrar ];
        config = {
          download_location = "/persist/deluge/downloads";
          move_completed = true;
          move_completed_path = "/data/downloads";
          copy_torrent_file = true;
          torrentfiles_location = "/persist/deluge/torrents";
          max_upload_slots_global = 100;
          max_connections_global = 2000;
          max_active_seeding = -1;
          max_active_downloading = 10;
          max_active_limit = -1;
          dht = false;
          utpex = false;
          listen_ports = [ 6400 6400 ];
          random_port = false;
          allow_remote = true;
          enabled_plugins = [ "Extractor" "Label" "Stats" "WebUi" ];
        };
      };

      services.jackett.enable = true;
      services.sonarr.enable = true;
      services.radarr.enable = true;

      users.users.sonarr.extraGroups = [ "users" ];
      users.users.radarr.extraGroups = [ "users" ];
    })).config.system.build.toplevel;
  };

  hexchen.dns.zones."chaoswit.ch".subdomains."torrents".CNAME = [ "${config.networking.hostName}.chaoswit.ch." ];
  services.nginx.recommendedProxySettings = true;
  services.nginx.virtualHosts."torrents.chaoswit.ch" = {
    forceSSL = true;
    enableACME = true;
    root = "/data/nfs";
    extraConfig = "autoindex on;";
    locations = {
      "= /".return = "404";
      "/jackett".proxyPass = "http://192.168.100.2:9117";
      "/sonarr".proxyPass = "http://192.168.100.2:8989";
      "/radarr".proxyPass = "http://192.168.100.2:7878";
    };
  };

  hexchen.dns.zones."chaoswit.ch".subdomains."plex".CNAME = [ "${config.networking.hostName}.chaoswit.ch." ];
  services.plex.enable = true;
  # yeah :/
  nixpkgs.config.allowUnfree = true;
  services.nginx.virtualHosts."plex.chaoswit.ch" = {
    forceSSL = true;
    enableACME = true;
    locations."/".proxyPass = "http://127.0.0.1:32400/";
  };

  hexchen.dns.zones."chaoswit.ch".subdomains."jellyfin".CNAME = [ "${config.networking.hostName}.chaoswit.ch." ];
  services.jellyfin.enable = true;
  services.nginx.virtualHosts."jellyfin.chaoswit.ch" = {
    forceSSL = true;
    enableACME = true;
    locations = {
      "/".proxyPass = "http://127.0.0.1:8096/";
      "/socket" = {
        proxyPass = "http://127.0.0.1:8096/";
        extraConfig = ''
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
      '';
      };
    };
  };
}
