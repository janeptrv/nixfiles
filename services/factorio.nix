{ config, lib, pkgs, ... }:

{
  services.factorio = {
    enable = true;
    description = "hexchen does factorio";
    openFirewall = true;
    game-password = "meow";
    game-name = "Lofycord builds a factory";
    requireUserVerification = false;
    autosave-interval = 10;
    admins = [ "hexchen" "jade" "OmniMancer" "nxmq" "nyanotech" ];
  };
}
