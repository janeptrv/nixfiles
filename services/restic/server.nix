{ config, lib, pkgs, ... }:

let
  port = 18685;
in {
  hexchen.dns.zones."chaoswit.ch".subdomains.restic.CNAME = [ "${config.networking.hostName}.chaoswit.ch." ];
  networking.firewall.allowedTCPPorts = [ port ];
  services.restic = {
    server = {
      enable = true;
      dataDir = "/data/restic";
      listenAddress = ":${toString port}";
      privateRepos = true;
      prometheus = true;
    };
  };
}
