{ config, lib, pkgs, ... }:

{
  services.pleroma = {
    enable = true;
    configs = [ (lib.fileContents ./config.exs) ];
    secretConfigFile = "/var/lib/pleroma/secret.exs";
    package = pkgs.pleroma-otp;
  };

  hexchen.dns.zones."chaoswit.ch".subdomains."pleroma".CNAME = [ "${config.networking.hostName}.chaoswit.ch." ];
  systemd.services.pleroma.environment = {
    DOMAIN = "pleroma.chaoswit.ch";
  };

  services.nginx.recommendedProxySettings = true;
  services.nginx.virtualHosts."pleroma.chaoswit.ch" = {
    enableACME = true;
    forceSSL = true;
    locations = {
      "/" = {
        proxyPass = "http://127.0.0.1:4000/";
        proxyWebsockets = true;
      };
    };
  };

  services.postgresql = {
    enable = true;
    ensureDatabases = [ "pleroma" ];
    ensureUsers = [
      {
        name = "pleroma";
        ensurePermissions."DATABASE pleroma" = "ALL PRIVILEGES";
      }
    ];
  };
}
