{ config, lib, pkgs, ... }:

{
  hexchen.deploy.groups = [ "dns" ];
  services.kresd.enable = lib.mkForce false;
  networking.policyrouting = {
    rules = [
      { rule = "sport 53 ipproto udp lookup main"; prio = 8300; }
    ];
  };
  hexchen.dns = {
    enable = true;
    dnssec = {
      enable = true;
      doSplitSigning = true;
    };
    symlinkZones = true;
    allZones = with pkgs.dns.combinators; let
      common = {
        SOA = {
          nameServer = "ns1.hxchn.de.";
          adminEmail = "admin@hxchn.de";
          serial = 2021081801;
          ttl = 3600;
        };
        CAA = map (x: x // { ttl = 3600; }) (letsEncrypt "acme@lilwit.ch");
      } // (lib.mapAttrs (name: value: map (x: x // { ttl = 3600; }) value) (delegateTo [ "ns1.hxchn.de." "ns2.hxchn.de." ]));
    in {
      "colon.at" = common;
      "chaoswit.ch" = common;
      "lilwit.ch" = common // {
        subdomains = {
          shell.A = [ (ttl 3600 (a "116.203.185.253")) ];
          shell.AAAA = [ (ttl 3600 (aaaa "2a01:4f8:c2c:6af7::1")) ];
          progress.A = [ (ttl 3600 (a "116.203.185.254")) ];
          progress.AAAA = [ (ttl 3600 (aaaa "2a01:4f8:c2c:7015::1")) ];
          ripped.A = [ (ttl 3600 (a "168.119.109.87")) ];
          world = {
            NS = [ (ttl 3600 (ns "shell.lilwit.ch.")) (ttl 3600 (ns "progress.lilwit.ch."))];
            DS = [{
              keyTag = 51762;
              algorithm = 13;
              digestType = 2;
              digest = "B384B510A40A6E335E9B8448784CC5A058EA140528983F8C01ABCA6B99054A7B";
              ttl = 3600;
            }];
          };
        };
      };
      "copyonwit.ch" = common;
      "h7.pm" = common;
      "7.3.3.1.0.c.a.4.f.0.a.2.ip6.arpa" = common;
    };
  };

  services.nsd.zones."7.3.3.1.0.c.a.4.f.0.a.2.ip6.arpa".dnssec = lib.mkForce false;
}
