{ config, lib, pkgs, ... }:

{
  services.coredns = {
    enable = true;
    config = ''
      . {
        bind 2a0f:4ac0:1337:64::1
        bind 127.0.0.1
        forward . 2606:4700:4700::1111
        dns64 2a0f:4ac0:1337:64::1/96
      }
    '';
  };

  systemd.services.tayga = (let
    config = pkgs.writeText "tayga.conf" ''
        tun-device tunnat64
        ipv4-addr 10.255.255.254
        prefix 2a0f:4ac0:1337:64::/96
        dynamic-pool 10.255.255.0/24
        strict-frag-hdr 1
      '';
    startScript = pkgs.writeScriptBin "tayga-start" ''
        #! ${pkgs.runtimeShell} -e
        ${pkgs.iproute}/bin/ip link set up tunnat64 || true
        ${pkgs.iproute}/bin/ip route add 10.255.255.0/24 dev tunnat64 || true
        ${pkgs.iproute}/bin/ip -6 route add 2a0f:4ac0:1337:64::/96 dev tunnat64 || true
        ${pkgs.tayga}/bin/tayga -d --config ${config}
      '';
  in {
    wantedBy = [ "multi-user.target" ];
    after = [ "network.target" ];
    serviceConfig = {
      ExecStart = ''${startScript}/bin/tayga-start'';
    };
  });

  networking.interfaces.tunnat64.virtual = true;

  boot.kernel.sysctl."net.ipv6.conf.all.forwarding" = true;
  boot.kernel.sysctl."net.ipv4.conf.all.forwarding" = true;

  networking.nat = {
    enable = true;
    internalInterfaces = [ "tunnat64" ];
    externalInterface = "cornbox";
  };
  networking.firewall.allowedUDPPorts = [ 53 ];
}
