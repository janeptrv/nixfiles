{ config, lib, pkgs, modules, ... }:

{
  imports = [
    modules.network.bird
    modules.network.policyrouting
    modules.network.nftables
  ];

  options.network = with lib; {
    routeDefault = mkOption {
      default = true;
      type = types.bool;
    };
    prefix4 = mkOption {
      default = "195.39.247";
      type = types.str;
    };
    prefix6 = mkOption {
      default = "2a0f:4ac0:1337";
      type = types.str;
    };
    prefixOffset = mkOption {
      default = 64;
      type = types.int;
    };
    prefsrc4 = mkOption {
      type = types.nullOr types.str;
      default = "${config.network.prefix4}.${toString (config.network.prefixOffset + config.network.wireguard.magicNumber)}";
    };
    prefsrc6 = mkOption {
      type = types.nullOr types.str;
      default = "${config.network.prefix6}::${toString config.network.wireguard.magicNumber}";
    };
    networks4 = mkOption {
      type = types.listOf types.str;
    };
    networks6 = mkOption {
      type = types.listOf types.str;
    };
  };

  config = let
    filters = {
      net4 = "net ~ [ ${lib.concatStringsSep ", " config.network.networks4} ]";
      net6 = "net ~ [ ${lib.concatStringsSep ", " config.network.networks6} ]";
    };
  in {
    network.networks4 = [ "195.39.247.64/27+" ];
    network.networks6 = [ "2a0f:4ac0:1337::/48+" ];
    networking.firewall.extraCommands = "ip6tables -A INPUT -p 89 -i wgmesh-+ -j ACCEPT";
    networking.nftables.extraInput = ''
      meta l4proto 89 iifname wgmesh-* accept
    '';

    networking.policyrouting = {
      enable = true;
      rules = [
        { rule = "lookup main suppress_prefixlength 0"; prio = 7000; }
        { rule = "lookup 89 suppress_prefixlength 0"; prio = 8000; }
        { rule = "from all fwmark 51820 lookup main"; prio = 9000; }
      ] ++ (lib.optional config.network.routeDefault { rule = "not from all fwmark 51820 lookup 89"; prio = 9000; });
    };

    network.wireguard = {
      enable = true;
      fwmark = 51820;
    };

    network.bird = let
      mkKernel = version: ''
        ipv${toString version} {
          import all;
          export filter {
            ${
              if (config.network."prefsrc${toString version}" or null) != null
              then "krt_prefsrc = ${config.network."prefsrc${toString version}"};"
              else ""
             }
            if source ~ [ RTS_STATIC, RTS_PIPE ] then reject;
            if proto = "kerneli${toString version}" then reject;
            accept;
          };
        };
        learn;
        kernel table 89;
        scan time 15;
      '';
      mkIgp = version: {
          version = 3;
          extra = ''
            ipv${toString version} {
              import all;
              export filter {
                return ${filters."net${toString version}"};
              };
            };
          '';
          areas."0".interfaces."wgmesh-*".cost = 100;
      };
    in {
      routerId = "172.23.1.${toString config.network.wireguard.magicNumber}";
      kernel4Config = mkKernel 4;
      kernel6Config = mkKernel 6;
      ospf = {
        enable = true;
        protocols.igp4 = mkIgp 4;
        protocols.igp6 = mkIgp 6;
      };
    };

    services.bird2.config = lib.mkAfter ''
      ipv4 table kernelimport4; ipv6 table kernelimport6;
      protocol kernel kerneli4 { learn; ipv4 { table kernelimport4; import all; export none; }; }
      protocol kernel kerneli6 { learn; ipv6 { table kernelimport6; import all; export none; }; }
      protocol pipe { table master4; peer table kernelimport4; import filter { return ${filters.net4}; }; export none; }
      protocol pipe { table master6; peer table kernelimport6; import filter { return ${filters.net6}; }; export none; }
    '';

    users.users.hexchen.extraGroups = [ "bird2" ];
  };
}
