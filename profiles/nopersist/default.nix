{ config, lib, pkgs, modules, ... }:

with lib;

{
  imports = [
    modules.bindmount
  ];

  users.mutableUsers = false;

  boot.initrd.postDeviceCommands = mkIf (config.fileSystems."/".fsType == "zfs") (mkAfter ''
    zfs rollback -r ${config.fileSystems."/".device}@blank
  '');

  services.openssh = {
    hostKeys = [
      {
        path = "/persist/ssh/ssh_host_ed25519_key";
        type = "ed25519";
      }
      {
        path = "/persist/ssh/ssh_host_rsa_key";
        type = "rsa";
        bits = 4096;
      }
    ];
  };

  boot.initrd.network.ssh.hostKeys = mkIf config.hexchen.encboot.enable (mkForce [ /persist/ssh/encboot_host ]);

  systemd.tmpfiles.rules =
    []
    ++ (optional (config.security.acme.certs != {}) "L /var/lib/acme - - - - /persist/var/lib/acme")
    ++ (optional config.hardware.bluetooth.enable "L /var/lib/bluetooth - - - - /persist/var/lib/bluetooth");

  systemd.services.acme-fixperms.serviceConfig.StateDirectory =
    mkIf (config.security.acme.certs != {})
      (mkForce ([ "acme/.lego" ] ++ (lib.mapAttrsToList (name: _: "acme/${name}") config.security.acme.certs)));

  hexchen.bindmounts =
    listToAttrs (map
      (service: nameValuePair
        "/var/lib/${service}"
        (mkIf config.services."${service}".enable "/persist/var/lib/${service}"))
      [ "pleroma" "yggdrasil" "jellyfin" "opendkim" "rspamd" "postfix" ])
    // (if config.services.opendkim.enable then { "/var/dkim" = "/persist/var/dkim"; } else {})
    // (if config.services.dovecot2.enable then { "/var/lib/dovecot" = "/persist/var/lib/dovecot"; } else {})
    // (if config.services.factorio.enable then { "/var/lib/private/factorio" = "/persist/var/lib/factorio"; } else {});

  environment.etc = {
    "wpa_supplicant.conf" = mkIf config.networking.wireless.enable { source = "/persist/etc/wpa_supplicant.conf"; };
  };

  services.postgresql.dataDir = "/persist/postgresql/${config.services.postgresql.package.psqlSchema}";
  services.hedgedoc.workDir = "/persist/var/lib/hedgedoc";
  services.jackett.dataDir = "/persist/jackett";
  services.sonarr.dataDir = "/persist/sonarr";
  services.radarr.dataDir = "/persist/radarr";
  services.plex.dataDir = "/persist/var/lib/plex";
}
