{ config, lib, pkgs, profiles, ... }:

{
  imports = [
    profiles.base
  ];
  boot.plymouth.enable = true;
  nixpkgs.config.allowUnfree = true;
  powerManagement.cpuFreqGovernor = lib.mkOverride 998 "ondemand";
  programs.light.enable = true;

  sound.enable = true;
  hardware.pulseaudio.enable = true;

  services.printing = {
    enable = true;
    drivers = with pkgs; [ foomatic-filters foo2zjs epson-escpr2 epson-escpr ];
    browsing = true;
  };
  hardware.printers.ensureDefaultPrinter = "Epson_ET-2710";
  hardware.printers.ensurePrinters = [{
    name = "Epson_ET-2710";
    description = "Epson ET-2710";
    location = "Home";
    deviceUri = "dnssd://EPSON%20ET-2710%20Series._pdl-datastream._tcp.local";
    model = "epson-inkjet-printer-escpr/Epson-ET-2710_Series-epson-escpr-en.ppd";
    ppdOptions.PageSize = "A4";
    ppdOptions.Ink = "COLOR";
    ppdOptions.MediaQuality = "PLAIN_NORMAL";
  }];

  networking.useDHCP = true;
  networking.dhcpcd.extraConfig = ''
    static domain_name_servers=1.1.1.1 1.0.0.1 2606:4700:4700::1111 2606:4700:4700::1001
  '';
  networking.wireless.enable = true;
  hardware.opengl.enable = true;

  services.avahi.enable = true;
  services.avahi.nssmdns = true;

  services.udev.packages = [ pkgs.yubikey-personalization pkgs.libu2f-host ];

  services.xserver.enable = true;
  # services.xserver.desktopManager.plasma5.enable = true;
  services.xserver.displayManager.sddm.enable = true;

  boot.supportedFilesystems = [ "zfs" ];

  environment.shellInit = ''
    export GPG_TTY="$(tty)"
    gpg-connect-agent /bye
  '';
}
