{ config, lib, pkgs, overlays, users, modules, ... }:

{
  imports = [
    users.hexchen.base
    users.kat
    users.rx14
    ./make-nixpkgs.nix
    modules.network.yggdrasil modules.network.wireguard
    modules.dns
    modules.network.nftables
  ];

  nixpkgs.overlays = [ overlays.hexpkgs overlays.dns ];

  nix = {
    binaryCaches = [ "https://kittywitch.cachix.org" ];
    binaryCachePublicKeys =
      [ "kittywitch.cachix.org-1:KIzX/G5cuPw5WgrXad6UnrRZ8UDr7jhXzRTK/lmqyK0=" ];
  };

# boot.kernelPackages = lib.mkDefault pkgs.linuxPackages_latest;
  boot.kernelPackages = lib.mkDefault pkgs.linuxPackages;
  boot.kernelParams = [ "quiet" ];

  networking.nftables.enable = lib.mkDefault true;
  networking.domain = lib.mkDefault "chaoswit.ch";
  network.yggdrasil.trust = true;

  services.journald.extraConfig = "SystemMaxUse=512M";
  nix.gc.automatic = lib.mkDefault true;
  nix.gc.options = lib.mkDefault "--delete-older-than 1w";
  nix.trustedUsers = [ "root" "@wheel" ];
  environment.variables.EDITOR = "vim";

  home-manager.useGlobalPkgs = true;
  programs.command-not-found.enable = false;

  services.openssh = {
    enable = true;
    ports = lib.mkDefault [ 54160 ];
    passwordAuthentication = false;
    challengeResponseAuthentication = false;
    permitRootLogin = lib.mkDefault "no";
    gatewayPorts = lib.mkDefault "yes";
    extraConfig = "StreamLocalBindUnlink yes";
  };
  programs.mosh.enable = true;
  security.sudo.wheelNeedsPassword = lib.mkDefault false;

  powerManagement.cpuFreqGovernor = lib.mkOverride 999 "performance";

  i18n.defaultLocale = "en_IE.UTF-8";
  time.timeZone = "UTC";
  console = {
    font = "Lat2-Terminus16";
    keyMap = "de";
  };

  programs.mtr.enable = true;

  environment.systemPackages = with pkgs; [
    kitty.terminfo
    htop tcpdump nload iftop bottom lm_sensors iperf
    usbutils pciutils binutils dnsutils
    cryptsetup gptfdisk

    ripgrep fd pv progress parallel file vim
    git rsync whois
    p7zip zstd
    gnupg pinentry-curses
  ];
}
