{ config, lib, pkgs, modules, ... }:

let
  colors = import ./colors.nix;
in {
  imports = [
    modules.hh3
  ];
  nixpkgs.config = {
    mumble.speechdSupport = true;
    allowUnfree = true;
  };

  services.tor = {
    enable = true;
    client.enable = true;
    client.dns.enable = true;
    torsocks.enable = true;
  };

  users.users.hexchen = {
    packages = with pkgs; [
      dino transmission-remote-gtk pulsemixer pavucontrol quasselClient
      firefox git kitty j4-dmenu-desktop bemenu
      breeze-qt5 i3blocks mako
      syncplay mpv youtube-dl
      wl-clipboard mumble hh3cord hh3cord-canary
      xdg_utils
      slurp grim libnotify
      thunderbird zathura # gnome3.nautilus
      # tex stuff for school
      texlive.combined.scheme-full
      openjdk
      _1password-gui pinentry-qt
      horizon-eda kicad-with-packages3d kicad-unstable
      deluge torbrowser
      binaryninja
    ];
    extraGroups = [ "video" "cdrom" ];
  };

  home-manager.users.hexchen = {
    home.file.".gnupg/gpg-agent.conf".text = ''
      enable-ssh-support
      pinentry-program ${pkgs.pinentry.qt}/bin/pinentry
    '';

    gtk = {
      enable = true;
      font = {
        name = "Fira Sans";
        package = pkgs.fira;
        size = 12;
      };
      iconTheme = {
        name = "elementary";
        package = pkgs.elementary-xfce-icon-theme;
      };
    };
    programs.kitty = {
      enable = true;
      font.name = "Iosevka Term";
      settings = {
        font_size = "12.0";
        background = colors.black;
        foreground = colors.white;
        selection_background = colors.white;
        selection_foreground = colors.black;
        url_color = colors.yellow;
        cursor = colors.white;
        active_border_color = "#75715e";
        active_tab_background = "#9900ff";
        active_tab_foreground = colors.white;
        inactive_tab_background = "#3a3a3a";
        inactive_tab_foreground = "#665577";
      } // colors.base16;
    };
    programs.beets = {
      enable = true;
      settings = {
        directory = "~/Music";
        library = "~/.local/share/beets.db";
        plugins = lib.concatStringsSep " " [ "bpd" "play" "duplicates" "chroma" ];
        play = {
          command = "${pkgs.mpv}/bin/mpv --no-audio-display";
          warning_threshold = 10000;
        };
      };
    };
    programs.hh3 = {
      enable = true;
      modules = [
        "betterCodeblocks" "channelleak"  "charcount" "commands" "copyAvatarUrl" "createEmoji" "dblClickEdit"
        "experiments" "fixmentions" "guildFeatures" "guildVideo" "hiddenTyping" "imageUrls" "loadingScreen"
        "noconmsg" "oldQuote" "popoutDates" "postnet" "premiumVideo" "pseudoscience" "quickDelete" "sentrynerf" "showTag" "tardid"
        "twemojiPatch" "allPresences" "callIdling" "callRingingBeat" "typingChannel" "consistentLayout" "legacyVoiceRegions" "noAgeGate"
        "popoutMutuals" "shortlinks" "ttsLinux" "whoJoined" "unravelMessage"
      ];
      config = {
        modules = {
          hiddenTyping.options.whenInvisible = true;
          loadingScreen.options.style = "random";
          premiumVideo.options.stereo = true;
          antiDelete.options.ignorePluralKit = true;
        };
      };
    };
  };
}
