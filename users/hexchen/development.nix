{ config, lib, pkgs, ... }:

{
  users.users.hexchen.packages = with pkgs; [
    crystal shards ameba
    go python38
    elixir lfe rebar3 elixir_ls erlang erlang-ls
    niv nixfmt patchelf
    flashrom ifdtool cbfstool nvramtool
    protobuf chromium
    rustup rust-analyzer
    kubernetes cilium-cli helm docker
  ];
  home-manager.users.hexchen = {
    services.lorri.enable = true;
  };

  virtualisation.docker.enable = true;
  users.users.hexchen.extraGroups = [ "docker" ];
}
