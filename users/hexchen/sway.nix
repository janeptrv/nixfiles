{ config, pkgs, lib, ... }:

let
  colors = import ./colors.nix;
in {
  fonts.fonts = with pkgs; [ font-awesome nerdfonts iosevka emacs-all-the-icons-fonts ];
  users.users.hexchen.packages = with pkgs; [ ];
  programs.sway.enable = true;

  systemd.user.services.mako = {
    serviceConfig.ExecStart = "${pkgs.mako}/bin/mako";
    restartTriggers = [
      config.home-manager.users.hexchen.xdg.configFile."mako/config".source
    ];
  };

  home-manager.users.hexchen = {
    programs.mako = {
      enable = true;
      defaultTimeout = 3000;
      borderColor = colors.white;
      backgroundColor = "${colors.black}70";
      textColor = colors.white;
    };
    wayland.windowManager.sway = {
      enable = true;
      config = let
        dmenu = "${pkgs.bemenu}/bin/bemenu --fn 'Iosevka 12' --nb '${colors.black}' --nf '${colors.white}' --sb '${colors.red}' --sf '${colors.white}' -l 5 -i";
        lockCommand = "swaylock -i ${./background.jpg} -s fill";
        cfg = config.home-manager.users.hexchen.wayland.windowManager.sway.config;
      in {
        bars = [{ command = "${pkgs.waybar}/bin/waybar"; }];

        output = let
          left = {
            res = "1680x1050";
            pos = "0 540";
            transform = "270";
          };
          top = {
            res = "3840x2160";
            pos = "1050 0";
          };
          bottom = {
            res = "1920x1080";
            pos = "2000 2160";
          };
          right = {
            res = "1680x1050";
            pos = "4890 540";
            transform = "270";
          };
        in {
          "*".bg = "${./background.jpg} fill";
          "eDP-1" = bottom;
          "DP-1" = top;
          "DP-2" = top;
          "DP-3" = left;
          "DP-5" = right;
        };

        input = {
          "*" = {
            xkb_layout = "de";
            xkb_options = "ctrl:nocaps";
          };
          "1241:41617:Holtek_Anne_Pro_2_(c18)_QMK" = {
            xkb_layout = "us";
          };
          "13:0:AnnePro2_P1_Keyboard" = {
            xkb_layout = "us";
          };
          "2:7:SynPS/2_Synaptics_TouchPad" = {
            dwt = "enabled";
            tap = "enabled";
            natural_scroll = "enabled";
            middle_emulation = "enabled";
            click_method = "clickfinger";
          };
        };

        fonts = {
          names = [ "Iosevka Nerd Font" ];
          size = 10.0;
        };
        terminal = "${pkgs.kitty}/bin/kitty";
        # TODO: replace with wofi
        menu = "${pkgs.j4-dmenu-desktop}/bin/j4-dmenu-desktop --dmenu=\"${dmenu}\" --term='${cfg.terminal}'";
        modifier = "Mod4";

        startup = [
          { command = "systemctl --user restart mako"; always = true; }
          { command = "${pkgs.swayidle}/bin/swayidle -w before-sleep '${lockCommand}'"; }
        ];

        window = {
          border = 1;
          titlebar = true;
        };

        keybindings = {
          "${cfg.modifier}+Return" = "exec ${cfg.terminal}";

          "${cfg.modifier}+Left" = "focus left";
          "${cfg.modifier}+Down" = "focus down";
          "${cfg.modifier}+Up" = "focus up";
          "${cfg.modifier}+Right" = "focus right";

          "${cfg.modifier}+Shift+Left" = "move left";
          "${cfg.modifier}+Shift+Down" = "move down";
          "${cfg.modifier}+Shift+Up" = "move up";
          "${cfg.modifier}+Shift+Right" = "move right";

          "${cfg.modifier}+Shift+space" = "floating toggle";
          "${cfg.modifier}+space" = "focus mode_toggle";

          "XF86AudioRaiseVolume" = "exec pactl set-sink-volume $(pacmd list-sinks |awk '/* index:/{print $3}') +5%";
          "XF86AudioLowerVolume" = "exec pactl set-sink-volume $(pacmd list-sinks |awk '/* index:/{print $3}') -5%";
          "XF86AudioMute" = "exec pactl set-sink-mute $(pacmd list-sinks |awk '/* index:/{print $3}') toggle";
          "XF86AudioMicMute" = "exec pactl set-source-mute $(pacmd list-sources |awk '/* index:/{print $3}') toggle";
          "XF86MonBrightnessDown" = "exec ${pkgs.light}/bin/light -U 5";
          "XF86MonBrightnessUp" = "exec ${pkgs.light}/bin/light -A 5";
          "${cfg.modifier}+Print" = "exec ${pkgs.bash}/bin/bash -c '~/.local/bin/elixiremanager.sh -w'";

          "${cfg.modifier}+d" = "exec ${cfg.menu}";
          "${cfg.modifier}+x" = "exec ${lockCommand}";

          "${cfg.modifier}+i" = "move workspace to output left";
          "${cfg.modifier}+o" = "move workspace to output right";
          "${cfg.modifier}+b" = "splith";
          "${cfg.modifier}+v" = "splitv";
          "${cfg.modifier}+s" = "layout stacking";
          "${cfg.modifier}+w" = "layout tabbed";
          "${cfg.modifier}+e" = "layout toggle split";
          "${cfg.modifier}+f" = "fullscreen";

          "${cfg.modifier}+Shift+q" = "kill";
          "${cfg.modifier}+Shift+c" = "reload";

          "${cfg.modifier}+r" = "mode resize";
          "${cfg.modifier}+Delete" = "mode \"System (l) lock, (e) logout, (s) suspend, (h) hibernate, (r) reboot, (Shift+s) shutdown\"";
        } // (lib.foldl lib.recursiveUpdate {} (map (workspace: {
          "${cfg.modifier}+${workspace}" = "workspace ${workspace}";
          "${cfg.modifier}+Shift+${workspace}" = "move container to workspace ${workspace}";
        }) [ "0" "1" "2" "3" "4" "5" "6" "7" "8" "9" ]));

        keycodebindings = {
          "--no-repeat 107" = "exec dbus-send --session --type=method_call --dest=net.sourceforge.mumble.mumble / net.sourceforge.mumble.Mumble.startTalking";
          "--release 107" = "exec dbus-send --session --type=method_call --dest=net.sourceforge.mumble.mumble / net.sourceforge.mumble.Mumble.stopTalking";
        };

        modes = {
          "System (l) lock, (e) logout, (s) suspend, (h) hibernate, (r) reboot, (Shift+s) shutdown" = {
            "l" = "exec ${lockCommand}, mode default";
            "e" = "exec swaymsg exit, mode default";
            "s" = "exec systemctl suspend, mode default";
            "h" = "exec systemctl hibernate, mode default";
            "r" = "exec systemctl reboot, mode default";
            "Shift+s" = "exec systemctl shutdown, mode default";
            "Return" = "mode default";
            "Escape" = "mode default";
          };
        };

        colors = {
          focused = {
            border = colors.bright.black;
            background = colors.bright.green;
            text = colors.black;
            indicator = colors.green;
            childBorder = colors.bright.black;
          };
          focusedInactive = {
            border = colors.bright.black;
            background = colors.green;
            text = colors.black;
            indicator = colors.green;
            childBorder = colors.bright.black;
          };
          unfocused = {
            border = colors.bright.black;
            background = colors.black;
            text = colors.bright.black;
            indicator = colors.bright.black;
            childBorder = colors.bright.black;
          };
          urgent = {
            border = colors.bright.black;
            background = colors.bright.red;
            text = colors.black;
            indicator = colors.red;
            childBorder = colors.bright.black;
          };
        };
      };
      wrapperFeatures.gtk = true;
      extraConfig = ''
        seat seat0 xcursor_theme breeze_cursors 20
        bindswitch --reload --locked lid:on output eDP-1 disable
        bindswitch --reload --locked lid:off output eDP-1 enable
      '';
    };
    programs.waybar = {
      enable = true;
      #       style = ''
      #         * {
      #           font-family: Iosevka;
      #         }
      #       '';
      settings = [{
        modules-left = [ "sway/workspaces" "sway/mode" ];
        modules-center = [ "sway/window" ];
        modules-right = [ "pulseaudio" "network" "cpu" "memory" "temperature" "battery" "clock" "tray" ];

        modules = {
          battery = {
            states = {
              good = 95;
              warning = 30;
              critical = 15;
            };
            format = "{capacity}% {icon}";
            format-charging = "{capacity}% ";
            format-plugged = "{capacity}% ";
            format-alt = "{time} {icon}";
            format-icons = ["" "" "" "" ""];
          };
          network = {
            format-wifi = "{essid} ({signalStrength}%) ";
            format-ethernet = "{ifname}: {ipaddr}/{cidr} ";
            format-linked = "{ifname} (No IP) ";
            format-disconnected = "Disconnected ⚠";
            format-alt = "{ifname}: {ipaddr}/{cidr}";
          };
        };
      }];
    };
  };
}
