{ config, lib, pkgs, hosts ? {}, ... }:

{
  imports = [
    ./fish.nix
  ];

  users.users.hexchen = {
    uid = 1000;
    isNormalUser = true;
    extraGroups = [ "wheel" "systemd-journal" ];
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINJ0tCxsEilAzV6LaNpUpcjzyEn4ptw8kFz3R+Z3YjEF hexchen@backup"
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDI3T1eFS77URHZ/HVWkMOqx7W1U54zJtn9C7QWsHOtyH72i/4EVj8SxYqLllElh1kuKUXSUipPeEzVsipFVvfH0wEuTDgFffiSQ3a8lfUgdEBuoySwceEoPgc5deapkOmiDIDeeWlrRe3nqspLRrSWU1DirMxoFPbwqJXRvpl6qJPxRg+2IolDcXlZ6yxB4Vv48vzRfVzZNUz7Pjmy2ebU8PbDoFWL/S3m7yOzQpv3L7KYBz7+rkjuF3AU2vy6CAfIySkVpspZZLtkTGCIJF228ev0e8NvhuN6ZnjzXxVTQOy32HCdPdbBbicu0uHfZ5O7JX9DjGd8kk1r2dnZwwy/ hexchen@yubi5"
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC4CLJ+mFfq5XiBXROKewmN9WYmj+79bj/AoaR6Iud2pirulot3tkrrLe2cMjiNWFX8CGVqrsAELKUA8EyUTJfStlcTE0/QNESTRmdDaC+lZL41pWUO9KOiD6/0axAhHXrSJ0ScvbqtD0CtpnCKKxtuOflVPoUGZsH9cLKJNRKfEka0H0GgeKb5Tp618R/WNAQOwaCcXzg/nG4Bgv3gJW4Nm9IKy/MwRZqtILi8Mtd+2diTqpMwyNRmbenmRHCQ1vRw46joYkledVqrmSlfSMFgIHI1zRSBXb/JkG2IvIyB5TGbTkC4N2fqJNpH8wnCKuOvs46xmgdiRA26P48C2em3 hexchen@yubi5c"
    ];
    shell = pkgs.fish;
    packages = with pkgs; [
      htop bottom ripgrep fd wget kitty.terminfo nmap rink
      python3
    ];
  };

  home-manager.users.hexchen = {
    xdg.configFile."nixpkgs/config.nix".text = "builtins.fromJSON \"${lib.replaceStrings ["\""] ["\\\""] (builtins.toJSON config.nixpkgs.config)}\"";

    programs.vim = {
      enable = true;
      extraConfig = ''
        set viminfo='20,<1000
        set mouse=a
      '';
    };

    programs.git = {
      enable = true;
      package = pkgs.gitAndTools.gitFull;
      userName = "hexchen";
      userEmail = "hexchen@lilwit.ch";
      signing = {
        key = "B1DF5EAD";
      };
      extraConfig = {
        pull.rebase = true;
        sendemail = {
          smtpserver = "koryaksky.chaoswit.ch";
          smtpuser = "hexchen@lilwit.ch";
          smtpencryption = "tls";
          smtpserverport = 587;
        };
      };
    };

    programs.bat.enable = true;
    programs.jq.enable = true;
    programs.ssh = {
      enable = true;
      controlMaster = "auto";
      controlPersist = "10m";
      hashKnownHosts = true;
      matchBlocks = let
        hexchen = {
          forwardAgent = true;
          extraOptions = {
            RemoteForward = "/run/user/1000/gnupg/S.gpg-agent /run/user/1000/gnupg/S.gpg-agent.extra";
          };
        };
        nixosHosts = with pkgs.lib; mapAttrs' ( name: host: {
          name = "${host.config.hexchen.deploy.ssh.host}";
          value = {
            port = host.config.hexchen.deploy.ssh.port;
          } // hexchen;
        }) (filterAttrs (name: host: host.config.hexchen.deploy.enable) hosts);
      in {
        "*.hacc.space" = hexchen // { port = 62954; };
        "*.hacc.media" = hexchen // { port = 62954; };
        "*.lilwit.ch" = hexchen // { port = 54160; };
        "libocedrus.infra4future.de" = { user = "root"; port = 8947; };
        "schleuse" = {
          hostname = "192.168.2.2";
          extraOptions = {
            KexAlgorithms = "+diffie-hellman-group1-sha1";
          };
        };
        "10.53.118.*" = {
          identityFile = "~/.ssh/container_ed25519";
        };
      } // nixosHosts;
    };
  };
}
