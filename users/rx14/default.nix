{ config, lib, pkgs, ... }:

{
  users.users.rx14 = {
    uid = 1015;
    isNormalUser = true;
    extraGroups = [ "wheel" ];
    openssh.authorizedKeys.keys = [
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCdoNnEPPeTm0xxJPfuGV9Zm634V4/4wGs9/sl4t2edcm2CxuJ/O4gIXi3EhfxO9pkHMzS0Kh33nL4NugG5VYOayrXTs2uhMZWmlkW1iEOljwo/nyp/R+wAbtoij8bX3lMOdpCaTdlQuPiAjUIvMbKHhqZAgfK+lDOb/AvFq1sjqb+MjHrzQXhXY0XYg2x335FWR8BxI3DOwde0n6KGhqObfA3gqgbhYa7gDGDH3Z2Lj7QdMQRK34fImGXTutTcGIoIPsd02gdiPxbugk4nf7q+PAsGJa7UyzHcUs7p9yayEsXNNlk/PWPYQzls7vNU0jabPXKeunkhS4OlNBSD4erYcwL5fKT2xXX0jzOHHHhLIYzs2pwNxkHzPZrHeATNMFSxqSzCxoAAt9JHSOj07kntWVaoCet+C1xFNNeNFo9geq2q0qL0X8nMaJ+quNFW/hhrJW06o1nDpgrLOS4C0o3EQes4xsWtV79XGxGkXN6FQAo5uZGMILiYU8JN3xG0kd0VQXjtmYeyTRTNZjCORTII/mngSDjX07m67n+n0CBn5xOxOA03FWDPfoelFwUseUaeFaQKFDHkL1r6H/p5kZTtipCMgOPAaKR4/ztHSWtkEwTqHSd3k7WH8buoOIY/3ivOGa9gxfQ0Nas0PVtXO026e0KmxrVIb8QLLBUempRSsQ== rx14"
    ];
  };
}
