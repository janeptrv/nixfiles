{ config, lib, pkgs, ... }:

{
  users.users.dragonmux = {
    uid = 1016;
    isNormalUser = true;
    extraGroups = [ "wheel" ];
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILC9OVngUSwKeFM+AdFQQOHWqBA2jFZJIiX2cEZfUsdq dx-mon@shiro"
    ];
  };
}
