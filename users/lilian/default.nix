{ pkgs, config, ... }:

{
  users.users.lilian = {
    openssh.authorizedKeys.keys = [
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDOl79sD8S7ybEkQ+V4gjPF2hlWeFpcTp3XW44/ky5/oQlVQ6leWqygIV5LPq4xOoRHemNOPaLWhmpebTdTidp7KdG7D0QE7cBUGHXtxiVbfEWqs4vwudgbK2r4RPVuD0Xw811VeUndEiXlHEuy99AMQ9RDYIp2W1McK9EtBVwG60WsltuQT+OYsP4mowl4ywFLSAL3/I/ukXwrJnVqe1icXLpHrJnoMqIqIuu4XrLjhKKduxE/R5gvJMeGK99q1HyiG0eXskGiU0lcWH6mhGuP4SCWPD7xF53XHw48hb0WzOR63UmtL6Q8rrsBfEGMajoS+rzpwAlAf7pOnpAyVQDLLr+uXdFweatLyEUR1Eqk8gJHiTMAyqmqloynProDytdjD1JGFeZF1diwECrxnPlJ0TqsBwMTyhrXyMTB33Q1WhPgdcq4E24a2ukgIkU47G6tp6PUJukVFyTnFfeGchm2eWBcD1NamuQUuwN20mZ1amzSuKdGWeUHSofcJuLj0H3QvYu3t2ysml8gjJFJ3AjLH8mP/+9AwmZokQ5OwQmZHbbjlsMxmVdo6tTbjAjdrSBuyk4YAHwb9l8M7wG0DcOdeW83+6PFSgfCTdtNIIFROISy1JYI4xA7ATzb5X2/GpxzZJop7aIstVYIDaiHa2gnD7Vw94+mi8jXQrfq10jAIw=="
    ];
    isNormalUser = true;
    shell = pkgs.zsh;
    uid = 1012;
    extraGroups = [ "wheel" "lxd" "docker" "libvirtd" ];
  };
}
