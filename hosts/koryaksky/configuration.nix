{ config, lib, pkgs, profiles, users, modules, ... }:

{
  imports = [
    ./hardware.nix
    profiles.server profiles.nopersist
    profiles.network
    users.dragonmux
    ../../services/pleroma
    ../../services/codimd.nix
    ../../services/torrents/koryaksky.nix
    ../../services/hcf.nix
    ../../services/mail.nix
    ../../services/factorio.nix
    ../../services/gitlab-runner.nix
    modules.nftnat modules.network.wireguard
  ];

  hexchen.encboot = {
    enable = true;
    dataset = "rpool";
    networkDrivers = [ "igb" ];
  };

  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  boot.loader.grub.devices = [ "/dev/nvme0n1" "/dev/nvme1n1" ];
  boot.supportedFilesystems = [ "zfs" ];

  networking.hostId = "3b905115";

  networking.useDHCP = true;
  networking.interfaces.enp35s0.ipv6.addresses = [ {
    address = "2a01:4f9:3a:3bd7::1";
    prefixLength = 64;
  } ];
  networking.defaultGateway6 = {
    address = "fe80::1";
    interface = "enp35s0";
  };

  hexchen.dns.zones."chaoswit.ch".subdomains = {
    koryaksky.AAAA = [ (lib.head config.networking.interfaces.enp35s0.ipv6.addresses).address ];
    koryaksky.A = [ "135.181.219.168" ];
    "koryaksky.hetzner".A = [ "135.181.219.168" ];
  };

  services.restic.backups.tardis = {
    passwordFile = "/persist/restic/system";
    paths = [
      "/home"
      "/persist"
    ];
    pruneOpts = [
      "--keep-daily 7"
      "--keep-weekly 5"
      "--keep-monthly 12"
    ];
    repository = "";
  };
  systemd.services."restic-backups-tardis".environment.RESTIC_REPOSITORY_FILE = "/persist/restic/system.repo";

  network.yggdrasil = {
    enable = true;
    dns.enable = true;
    listen = {
      enable = true;
      endpoints = [ "tcp://135.181.219.168:9812" ];
    };
    pubkey = "fdd65d1e354d67d83ce29918e7f0a2c8986f953a2a3c5bf94467e8ef85b3c24a";
  };
  network.wireguard = {
    magicNumber = 1;
    pubkey = "tZl+tX1gAQDRrwL1bQ7wWNIGMhgY29w6IpH8/TvxYg4=";
    keyPath = "/persist/wireguard/mesh";
    publicAddress4 = "135.181.219.168";
  };

  network.routeDefault = false;
  network.prefsrc4 = "172.23.1.${toString config.network.wireguard.magicNumber}";
  network.prefsrc6 = null;

  networking.firewall.allowedTCPPorts = [ 22 9812 ];
  networking.nat.externalInterface = "enp35s0";
  networking.nat.internalInterfaces = [ "ve-+" ];
  hexchen.nftables.nat.enable = true;

  system.stateVersion = "21.05";
}
