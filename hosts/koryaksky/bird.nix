{ config, lib, pkgs, modules, ... }:

{
  imports = [
    modules.network.bird
  ];

  networking.nftables.extraInput = ''
    iifname "wgmesh-*" meta l4proto ospfigp accept
  '';

  network.bird = {
    routerId = "172.23.1.1";
    kernel4Config = ''
      ipv4 {
        import all;
        export filter {
          if source = RTS_STATIC then reject;
          accept;
        };
      };
      scan time 15;
    '';
    ospf = {
      enable = true;
      protocols."igp4" = {
        version = 3;
        extra = "ipv4 { import all; export all; };";
        areas."0".interfaces."wgmesh-*" = {};
      };
      protocols."igp6" = {
        version = 3;
        extra = "ipv6 { import none; export all; };";
        areas."0".interfaces."wgmesh-*" = {};
      };
    };
  };
}
