{config, pkgs, lib, ...}:

{
  networking.firewall.interfaces."wgmesh-*".allowedTCPPorts = [ 111 2049 config.services.netatalk.port ];
  networking.firewall.interfaces.yggdrasil.allowedTCPPorts = [ 111 2049 config.services.netatalk.port ];

  services.nfs.server.enable = true;
  services.nfs.server.exports = ''
    /data/cornbox 195.39.247.64/27(rw) 2a0f:4ac0:1337::/64(rw) 200::/7(ro) 2a0f:4ac0:1337:b000::/56(rw) 172.23.1.1/24(rw)
    /data/rips 195.39.247.64/27(rw) 2a0f:4ac0:1337::/64(rw) 200::/7(ro) 2a0f:4ac0:1337:b000::/56(rw) 172.23.1.1/24(rw)
    /data/torrents 135.181.219.168/32(rw) 195.39.247.64/27(rw) 2a0f:4ac0:1337::/64(rw) 200::/7(ro) 2a0f:4ac0:1337:b000::/56(rw) 172.23.1.1/24(rw)
  '';

  services.netatalk.enable = true;
  services.netatalk.settings = {
    cornbox = {
      path = "/data/cornbox";
    };
    torrents = {
      path = "/data/torrents";
    };
    rips = {
      path = "/data/rips";
    };
    "tardis" = {
      "time machine" = "yes";
      path = "/data/tardis";
      "valid users" = "hexchen";
    };
  };
}
