{ config, lib, pkgs, modules, ... }:

let
  addr4 = "195.39.247.68/28";
  addr6 = "2a0f:4ac0:1337::14/64";
in {
  imports = [
    modules.network.policyrouting
  ];

  networking.firewall.allowedUDPPorts = [ 51820 ];
  networking.wireguard.interfaces.cornbox = {
    privateKeyFile = "/etc/wireguard/cornbox.key";
    ips = [ addr4 addr6 ];
    listenPort = 51820;
    table = "51820";
    postSetup = "ip l set cornbox mtu 1500";
    peers = [
      {
        allowedIPs = [ "::/0" "0.0.0.0/0" ];
        publicKey = "8IWyiQL3wKP9CD/4UdS9b8mcbL67mkUyeSPORgEPvV0=";
        endpoint = "cornbox.hetzner.chaoswit.ch:51821";
      }
    ];
  };

  hexchen.dns.zones."chaoswit.ch".subdomains.storah = pkgs.dns.combinators.host "195.39.247.68" "2a0f:4ac0:1337::14";

  networking.policyrouting = {
    enable = true;
    rules6 = [
      { rule = "from ${addr6} lookup 51820"; prio = 7000; }
    ];
    rules4 = [
      { rule = "from ${addr4} lookup 51820"; prio = 7000; }
    ];
  };
}
