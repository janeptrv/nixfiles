{ config, lib, pkgs, profiles, ... }:

{
  imports = [
    profiles.base
    ./hardware.nix
  ];

  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  boot.loader.grub.device = "/dev/sda";

  powerManagement.cpuFreqGovernor = lib.mkOverride 998 "ondemand";
  networking.useDHCP = true;
  networking.wireless.enable = true;
  services.openssh.forwardX11 = true;

  environment.systemPackages = with pkgs; [
    whipper
  ];

  users.users.hexchen.extraGroups = [ "cdrom" ];

  network.yggdrasil = {
    enable = false;
    dns.enable = true;
    pubkey = "f041d8c329cb7b7c88bd9536e049b33349068585a8cc649c238974cb136b6b05";
  };

  networking.domain = "net.lilwit.ch";

  system.stateVersion = "21.05";
}
