{ config, lib, pkgs, modulesPath, ... }:

{
  imports =
    [ (modulesPath + "/installer/scan/not-detected.nix")
    ];

  boot.initrd.availableKernelModules = [ "xhci_pci" "ehci_pci" "ahci" "usb_storage" "sd_mod" "sdhci_pci" ];
  boot.initrd.kernelModules = [ "dm-snapshot" ];
  boot.kernelModules = [ "kvm-intel" ];
  boot.extraModulePackages = [ ];

  boot.initrd.luks.devices = {
      cryptroot = {
        device = "/dev/disk/by-uuid/70a7a197-b3a4-4c7a-8c0c-d5ffa62fcde2";
        preLVM = true;
      };
  };

  fileSystems."/" =
    { device = "/dev/disk/by-uuid/cf312e5d-60c7-4ffe-814a-d416596ff057";
      fsType = "btrfs";
      options = [ "subvol=@root" ];
    };

  fileSystems."/home" =
    { device = "/dev/disk/by-uuid/cf312e5d-60c7-4ffe-814a-d416596ff057";
      fsType = "btrfs";
      options = [ "subvol=@home" ];
    };

  fileSystems."/persist" =
    { device = "/dev/disk/by-uuid/cf312e5d-60c7-4ffe-814a-d416596ff057";
      fsType = "btrfs";
      options = [ "subvol=@persist" ];
    };

  fileSystems."/nix" =
    { device = "/dev/disk/by-uuid/cf312e5d-60c7-4ffe-814a-d416596ff057";
      fsType = "btrfs";
      options = [ "subvol=@store" ];
    };

  fileSystems."/boot" =
    { device = "/dev/disk/by-uuid/F587-247C";
      fsType = "vfat";
    };

  swapDevices =
    [ { device = "/dev/disk/by-uuid/a3342b13-e9e9-47fd-b345-2b004645d6cb"; }
    ];

  powerManagement.cpuFreqGovernor = lib.mkDefault "powersave";
}
