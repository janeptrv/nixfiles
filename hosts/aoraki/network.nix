{ config, lib, pkgs, modules, ... }:

{
  imports = [
    modules.nftnat
    modules.network.bird
  ];

  boot.blacklistedKernelModules = [ "ip_tables" "ip6_tables" "x_tables"];

  networking.useDHCP = lib.mkForce false;
  networking.interfaces.upstream.useDHCP = true;
  # TODO: hardcode manglement address
  networking.interfaces.manglement.useDHCP = true;
  networking.dhcpcd.extraConfig = ''
    noipv6rs
    interface upstream
      ipv6rs
      ia_na 1
      ia_pd 2 passthrough
  '';

  networking.interfaces.pubnet.ipv4.addresses = [{ address = "195.39.247.81"; prefixLength = 28; }];
  networking.interfaces.pubnet.ipv6.addresses = [{ address = "2a0f:4ac0:1337:666::1"; prefixLength = 64; }];
  networking.interfaces.privnet.ipv4.addresses = [{ address = "192.168.40.1"; prefixLength = 24; }];
  networking.interfaces.privnet.ipv6.addresses = [{ address = "2a0f:4ac0:1337:662::1"; prefixLength = 64; }];
  networking.interfaces.guest.ipv4.addresses = [{ address = "192.168.41.1"; prefixLength = 24; }];
  networking.interfaces.guest.ipv6.addresses = [{ address = "2a0f:4ac0:1337:663::1"; prefixLength = 64; }];
  networking.interfaces.passthrough.ipv4.addresses = [{ address = "192.168.51.1"; prefixLength = 24; }];

  network.bird.ospf.protocols.igp4.areas."0".interfaces."privnet".cost = 50;
  network.bird.ospf.protocols.igp4.areas."0".interfaces."pubnet".cost = 50;
  network.bird.ospf.protocols.igp6.areas."0".interfaces."privnet".cost = 50;
  network.bird.ospf.protocols.igp6.areas."0".interfaces."pubnet".cost = 50;
  networking.nftables.extraInput = ''
    meta l4proto 89 iifname { pubnet, privnet } accept
  '';

  boot.kernel.sysctl = {
    "net.ipv6.conf.default.accept_ra" = 0;
    "net.ipv6.conf.all.forwarding" = true;
    "net.ipv4.conf.all.forwarding" = true;
  };

  networking.vlans = let
    vlans = {
      manglement = 10;
      upstream = 20;
      ioshit = 30;
      privnet = 40;
      guest = 41;
      # fritzbox = 50;
      passthrough = 51;
      pubnet = 60;
    };
  in lib.mapAttrs (_: id: { inherit id; interface = "enp3s0"; }) vlans;

  services.dhcpd4 = {
    enable = true;
    authoritative = true;
    extraConfig = ''
      option domain-name-servers 1.1.1.1, 1.0.0.1;
      subnet 195.39.247.80 netmask 255.255.255.240 {
        option broadcast-address 195.39.247.95;
        option routers 195.39.247.81;
        option subnet-mask 255.255.255.240;
        range 195.39.247.82 195.39.247.94;
      }
      subnet 192.168.40.0 netmask 255.255.255.0 {
        option routers 192.168.40.1;
        option subnet-mask 255.255.255.0;
        option broadcast-address 192.168.40.255;
        range 192.168.40.10 192.168.40.240;
      }
      subnet 192.168.41.0 netmask 255.255.255.0 {
        option routers 192.168.41.1;
        option subnet-mask 255.255.255.0;
        option broadcast-address 192.168.41.255;
        range 192.168.41.10 192.168.41.240;
      }
      subnet 192.168.51.0 netmask 255.255.255.0 {
        option routers 192.168.51.1;
        option subnet-mask 255.255.255.0;
        option broadcast-address 192.168.51.255;
        range 192.168.51.10 192.168.51.240;
      }
    '';
    interfaces = [ "pubnet" "privnet" "guest" "passthrough" ];
  };

  services.radvd = {
    enable = true;
    config = lib.concatMapStringsSep "\n" (interface: ''
      interface ${interface} {
        prefix ::/64 {};
        AdvSendAdvert on;
      };
    '') [ "pubnet" "privnet" "guest" "passthrough" ];
  };

  networking.nftables.extraForward = ''
    udp dport 53 ip daddr 195.39.247.81/28 drop
    meta nfproto ipv6 tcp flags syn tcp option maxseg size 1325-65535 tcp option maxseg size set 1324
    meta nfproto ipv4 tcp flags syn tcp option maxseg size 1345-65535 tcp option maxseg size set 1344

    # restrict guest access to only upstream networks
    iifname guest oifname { manglement, privnet, ioshit, passthrough } reject
    # restrict manglement access to "privileged" networks
    iifname != { manglement, privnet, pubnet } oifname manglement reject
  '';

  # NAT config.
  # TODO: merge nat into nftables module
  networking.nftables.extraConfig = ''
    table ip nat {
      chain postrouting {
        type nat hook postrouting priority 100
        # tunneled NAT for non-public addresses
        oifname "wgmesh-*" ip saddr != 195.39.247.64/27 snat to 195.39.247.81
        # untunnelled NAT
        oifname upstream masquerade
      }
    }
  '';

  # Passthrough should be passed through the main table
  networking.policyrouting.rules = [ { rule = "iif passthrough lookup main"; prio = 8100; } ];

  services.avahi = {
    enable = true;
    openFirewall = true;
    reflector = true;
    interfaces = [ "pubnet" "privnet" "passthrough" "ioshit" ];
    nssmdns = true;
  };
}
