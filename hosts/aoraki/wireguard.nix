{ config, lib, pkgs, profiles, ... }:

{
  imports = [
    profiles.network
  ];

  network.wireguard = {
    magicNumber = 4;
    pubkey = "CKggSP0qk3ejulOwDwVSp/DZ1Pbl29zcN1fJ47DMSEU=";
  };

  network.bird = {
    staticRoutes4 = [
      "195.39.247.80/28 blackhole"
    ];
    staticRoutes6 = [
      "2a0f:4ac0:1337:600::/56 blackhole"
    ];
    ospf.protocols.igp4.areas."0".interfaces."wgmesh-*".cost = lib.mkForce 200;
  };

  networking.policyrouting = {
    rules4 = [
      { rule = "to 176.9.20.102/32 lookup main"; prio = 8100; }
      { rule = "from 192.168.178.1/24 lookup main"; prio = 8100; }
    ];
  };

  network.prefsrc4 = "195.39.247.81";
  network.prefsrc6 = "2a0f:4ac0:1337:666::1";
}
