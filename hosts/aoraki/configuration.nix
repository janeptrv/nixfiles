{ config, lib, pkgs, users, profiles, ... }:

{
  imports = [
    ./hardware.nix
    ./network.nix
    ./wireguard.nix
    users.hexchen.base
    profiles.server
  ];

  hexchen.deploy.substitute = false;

  boot.loader.grub.enable = true;
  boot.loader.grub.efiSupport = true;
  boot.loader.grub.device = "nodev";

  network.yggdrasil = {
    enable = true;
    dns.enable = true;
    pubkey = "8fb6267eab3edeae7a0bf14c326ddc361c81cf41bead7c72c1eef08fa1e40b61";
  };

  hexchen.dns.zones."chaoswit.ch".subdomains = {
    aoraki.AAAA = [ "2a0f:4ac0:1337:666::1" ];
    aoraki.A = [ "195.39.247.81" ];
  };

  services.xserver.enable = true;
  services.xserver.desktopManager.kodi.enable = true;
  services.xserver.displayManager.lightdm.enable = true;
  services.xserver.displayManager.autoLogin.enable = true;
  services.xserver.displayManager.autoLogin.user = "hexchen";

  nixpkgs.overlays = [
    (self: pkgs: {
      vaapiIntel = pkgs.vaapiIntel.override { enableHybridCodec = true; };
    })
  ];
  hardware.opengl = {
    enable = true;
    extraPackages = with pkgs; [
      intel-media-driver # LIBVA_DRIVER_NAME=iHD
      vaapiIntel         # LIBVA_DRIVER_NAME=i965 (older but works better for Firefox/Chromium)
      vaapiVdpau
      libvdpau-va-gl
    ];
  };

  fileSystems."/nfs" = {
    device = "storah.chaoswit.ch:/data/torrents";
    fsType = "nfs";
    options = [ "x-systemd.automount" "noauto" ];
  };

  system.stateVersion = "21.05";
}
