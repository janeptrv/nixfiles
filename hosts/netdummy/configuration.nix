{ config, lib, pkgs, sources, modules, overlays, ... }:

with lib;

{
  imports = [
    modules.dns
    modules.network.yggdrasil
  ];
  nixpkgs.overlays = [ overlays.dns ];

  hexchen.deploy.enable = false;

  network.yggdrasil = {
    enable = true;
    pubkey = "0000000000000000000000000000000000000000000000000000000000000000";
    listen.enable = true;
    listen.endpoints = [];
    extra.pubkeys = {
      satorin = "53d99a74a648ff7bd5bc9ba68ef4f472fb4fb8b2e26dfecea33c781f0d5c9525";
      shanghai = "0cc3c26366cbfddfb1534b25c5655733d8f429edc941bcce674c46566fc87027";

      samhain = "a7110d0a1dc9ec963d6eb37bb6922838b8088b53932eae727a9136482ce45d47";
      yule = "9779fd6b5bdba6b9e0f53c96e141f4b11ce5ef749d1b9e77a759a3fdbd33a653";
      athame = "55e3f29c252d16e73ac849a6039824f94df1dee670c030b9e29f90584f935575";
      beltane = "d3e488574367056d3ae809b678f799c29ebfd5c7151bb1f4051775b3953e5f52";
      daiyousei = "89771aa2f15fce6bbc3548f95be360cf59657d299837b10adf53944b54e8f121";
      rinnosuke = "d3db7b089f3cb2d33e18c77b8f9a5a08185798143822b219dbc938aa37d29310";
      shinmyoumaru = "5ba8c9f8627b6e5da938e6dec6e0a66287490e28084e58125330b7a8812cc22e";
    };
  };
}
