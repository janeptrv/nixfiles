{ config, lib, pkgs, modulesPath, ... }:

{
  imports = [ (modulesPath + "/installer/scan/not-detected.nix") ];

  boot.initrd.availableKernelModules = [ "xhci_pci" "ehci_pci" "ahci" "sd_mod" "rtsx_pci_sdmmc" ];
  boot.initrd.kernelModules = [ "dm-snapshot" ];
  boot.kernelModules = [ "kvm-intel" ];
  boot.extraModulePackages = [ ];

  
  boot.initrd.luks.devices = {
      cryptroot = {
        device = "/dev/disk/by-uuid/e779bf1d-c946-46b1-8e51-fbfbe57e134f";
        preLVM = true;
      };
  };

  fileSystems."/" =
    { device = "/dev/disk/by-uuid/2685be01-0136-43ff-8687-290936e9635f";
      fsType = "btrfs";
      options = [ "subvol=@nix" ];
    };

  fileSystems."/boot" =
    { device = "/dev/disk/by-uuid/150B-A7BB";
      fsType = "vfat";
    };

  swapDevices =
    [ { device = "/dev/disk/by-uuid/d0e5aedb-95f9-4853-ad48-a80ac50377b9"; }
    ];

  nix.maxJobs = lib.mkDefault 4;
  powerManagement.cpuFreqGovernor = lib.mkDefault "powersave";
}
