{ config, pkgs, lib, sources, modules, ... }:

with lib;

let 
  addrs4 = [ { address = "176.9.20.102"; prefixLength = 24; } ];
  addrs6 = config.networking.interfaces.${config.networking.defaultGateway6.interface}.ipv6.addresses;
  concatMapStrings = strings.concatMapStrings;
in {

  networking.wireguard.interfaces.upstream-pbb = {
    privateKeyFile = "/etc/wireguard/upstream-pbb.key";
    ips = [ "195.39.247.64/32" "2a0f:4ac0:1337::1/128" ];
    listenPort = 51820;
    postSetup = "wg set upstream-pbb fwmark 51820 && ip link set upstream-pbb mtu 1500";
    table = "89";
    peers = [{
      allowedIPs = [ "0.0.0.0/0" "::0/0" ];
#     endpoint = "195.39.221.190:51820";
#     publicKey = "ygGp0FG7qjFsikd4Dv/68olRFKIQDdt0xrpcoac8YE8=";
      endpoint = "5.199.141.154:51820";
      publicKey = "kih/GnR4Bov/DM/7Rd21wK+PFQRUNH6sywVuNKkUAkk=";
    }];
  };
  networking.firewall.allowedUDPPorts = [ 51820 51821 ];

  hexchen.dns.zones."chaoswit.ch".subdomains.cornbox.A = [ "195.39.247.64" ];

  networking.policyrouting = {
    rules6 = (map (ip: { rule = "from ${ip.address} lookup main"; prio = 8500; }) addrs6);
    rules4 = (map (ip: { rule = "from ${ip.address} lookup main"; prio = 8500; }) addrs4)
             ++ [ { rule = "to 135.181.219.168 lookup main"; prio = 8500; } ];
  };

  boot.kernel.sysctl."net.ipv6.conf.all.forwarding" = true;
  boot.kernel.sysctl."net.ipv4.conf.all.forwarding" = true;
  boot.kernel.sysctl."net.ipv6.route.max_size" = 2147483647;

  networking.wireguard.interfaces.downstreams = {
    generatePrivateKeyFile = true;
    privateKeyFile = "/etc/wireguard/downstreams.key";
    ips = [ "195.39.247.65/32" "2a0f:4ac0:1337::1/128" ];
    listenPort = 51821;
    postSetup = "ip link set downstreams mtu 1500";
    peers = [
      {
        # nixda
        allowedIPs = [ "195.39.247.67/32" "2a0f:4ac0:1337::12/128" ];
        publicKey = "gSBD1yR99k5ztQen9t+fKZ/TnP+KXMPSZ3h/cNg9fEE=";
        persistentKeepalive = 25;
      }
      {
        # txsbcct
        allowedIPs = [ "195.39.247.69/32" "2a0f:4ac0:1337::15/128" ];
        publicKey = "/aFjGe+w3+7cL4P6PjLumpckDekZaO7aJdimgDEKUQc=";
        persistentKeepalive = 25;
      }
      {
        # asdf
        allowedIPs = [ "195.39.247.70/32" "2a0f:4ac0:1337::16/128" ];
        publicKey = "hkzgsCxz4rFi/T8LoOZ1q/vmoYlAa76+1lr8AzgzvDc=";
      }
      {
        # 2pow6
        allowedIPs = [ "195.39.247.71/32" "2a0f:4ac0:1337:64::/96" ];
        publicKey = "iIr0a08g3uNsaUG16Q4/6eTOgE6bFM8H3BJwNgWKJgQ=";
        endpoint = "2pow6.chaoswit.ch:51820";
      }
    ];
  };

  network.wireguard = {
    magicNumber = 3;
    pubkey = "vdULnq3UGjlcOqjmawwzyBFwSp8zFPHA61Z8Uxdp0T8=";
    publicAddress4 = "176.9.20.102";
  };
  networking.firewall.checkReversePath = false;
}
