{ config, pkgs, lib, profiles, modules, ... }:

let
  addr4 = "195.39.247.65";
  addr6 = "2a0f:4ac0:1337::1";
in {
  imports = [
    profiles.server
    profiles.network
    modules.network.bird
    ./hardware-configuration.nix
    ./wireguard.nix
    ../../services/zfs.nix
    ../../services/mail.nix
    ../../services/dns
  ];

  services.postgresql.enable = true;

  hexchen.encboot = {
    enable = true;
    networkDrivers = [ "e1000e" ];
  };

  nix.gc.automatic = false;

  # Use the GRUB 2 boot loader.
  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  boot.loader.grub.devices = [ "/dev/sda" "/dev/sdb" "/dev/sdc" "/dev/sdd" ];
  boot.supportedFilesystems = [ "zfs" ];

  networking.hostName = "cornbox"; # Define your hostname.
  networking.hostId = "3cfe9b0e";

  networking.useDHCP = false;
  networking.interfaces.enp3s0.useDHCP = true;
  networking.interfaces.enp3s0.ipv6.addresses = [ {
    address = "2a01:4f8:150:145c::1";
    prefixLength = 64;
  } ];
  networking.defaultGateway6 = {
    address = "fe80::1";
    interface = "enp3s0";
  };
  networking.nameservers = [ "1.1.1.1"  "1.0.0.1" ];
  networking.nat.enable = true;
  networking.nat.internalInterfaces = ["ve-+"];
  networking.nat.externalInterface = "upstream-pbb";
  networking.nftables.enable = false;

  networking.wireguard.enable = true;

  networking.firewall.allowedTCPPorts = [ 80 443 31079 ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  networking.firewall.extraCommands = ''
    ip6tables -A INPUT -p 89 -i wgmesh-+ -j ACCEPT
    iptables -A INPUT -p tcp -m tcp --tcp-flags SYN,RST SYN -j TCPMSS --set-mss 1344
    iptables -A FORWARD -p tcp -m tcp --tcp-flags SYN,RST SYN -j TCPMSS --set-mss 1344
    iptables -A OUTPUT -p tcp -m tcp --tcp-flags SYN,RST SYN -j TCPMSS --set-mss 1344
    ip6tables -A INPUT -p tcp -m tcp --tcp-flags SYN,RST SYN -j TCPMSS --set-mss 1324
    ip6tables -A FORWARD -p tcp -m tcp --tcp-flags SYN,RST SYN -j TCPMSS --set-mss 1324
    ip6tables -A OUTPUT -p tcp -m tcp --tcp-flags SYN,RST SYN -j TCPMSS --set-mss 1324
  '';

  services.nginx = {
    enable = true;
    recommendedGzipSettings = true;
    recommendedOptimisation = true;
    recommendedProxySettings = true;
    recommendedTlsSettings = true;
  };

  services.nginx.virtualHosts = {
    "lilwit.ch" = {
      enableACME = true;
      forceSSL = true;
      locations."/gpg/hexchen.asc".return = "301 https://keys.openpgp.org/vks/v1/by-fingerprint/93EEC643F4D42E6D6F8DE5C1EB9F6084B1DF5EAD";
    };
  };
  hexchen.dns.zones."chaoswit.ch".subdomains.wsem.CNAME = [ "cornbox.chaoswit.ch." ];
  hexchen.dns.zones."lilwit.ch" = { inherit (config.hexchen.dns.zones."chaoswit.ch".subdomains.cornbox) A AAAA; };

  hexchen.dns.zones."chaoswit.ch".subdomains = {
    cornbox.AAAA = [ (lib.head config.networking.interfaces.enp3s0.ipv6.addresses).address ];
    "cornbox.hetzner".A = [ "176.9.20.102" ];
  };

  hexchen.dns.xfrIPs = [ addr4 ];

  network.yggdrasil = {
    enable = true;
    dns.enable = true;
    listen = {
      enable = true;
      endpoints = [ "tcp://176.9.20.102:31079" ];
    };
    pubkey = "d7edd0868599850a207df99d4e7229ab971da14bc36b638c59df9d56302c072d";
  };

  network.networks4 = [ "0.0.0.0/0-" ];
  network.networks6 = [ "::/0-" ];
  network.bird = {
    ospf = {
      protocols."igp4" = {
        areas."0".interfaces."wgmesh-aoraki".cost = 500;
      };
      protocols."igp6" = {
        areas."0".interfaces."wgmesh-aoraki".cost = 500;
      };
    };
  };

  network.prefsrc4 = addr4;
  network.prefsrc6 = addr6;

  networking.interfaces.lo.ipv4.addresses = lib.singleton { address = addr4; prefixLength = 32; };
  networking.interfaces.lo.ipv6.addresses = lib.singleton { address = addr6; prefixLength = 128; };

  system.stateVersion = "20.03";
}
