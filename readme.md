This is a fork of [hexchen's nixfiles](https://gitlab.com/hexchen/nixfiles).

### USAGE

use `nix build -f . deploy.<hostname>`, where hostname is the corresponding host/profile as specified in `./hosts`

### NOTES
- The original repo was, at the time of forking, largely undocumented. In my modifications to suit it to my own systems, I have added extra documentation as I have needed it.
- The original repo was unoficially licensed as MIT, so I have taken the liberty of introducing that license officially in this fork.