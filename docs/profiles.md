# profiles
this is the configuration common to all hosts, separated into different profiles

- base: packages and configuration common to all systems
- desktop, server: self explanatory.
- network: sets up network for static ip addressing (both v4 and v6) as well as firewall rules and basic wireguard settings
- nopersist: unknown, possibly related to deployment (temp setup files)

# users
this is the configuration for individual user accounts.

particularly, sets up user-based settings that would be present in `configuration.nix`, like user groups, openssh pubkeys, uid, or shell

for a full-featured user setup, refer to [this](../users/hexchen/default.nix)