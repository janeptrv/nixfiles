## configuration.nix

- main file
- specify `imports` which contains references to local files, as well as users and profiles
see:
[profiles](./profiles.md)
[users](./profiles.md#users)

- configure basic host settings:
    - boot loader
    - network
    - services (xserver, etc)
    - hardware specific options (opengl, extra drivers)
    - extra file systems (nfs, etc)
    - stateVersion

this file is extremely similar to a local configuration.nix file, except packages are located in the user and profile records.

## hardware-configuration.nix / hardware.nix

the same as a normal hardware-configuration.nix file.
specify `imports`, particulary a common occurrence to reference `modulesPath + "/installer/scan/not-detected.nix"`

## miscellaneous nixfiles
extra host-specific configuration.