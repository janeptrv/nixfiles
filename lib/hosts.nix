# This file serves to resolve hostname -> host configuration
# this includes:
# - configuration.nix
# - deploy groups
# - networking, wireguard, hardware config, etc.
{
  pkgs,
  hostsDir ? ../hosts,
  commonImports ? [],
  pkgsPath ? ../pkgs,
  modules ? {},
  overlays ? {},
  profiles ? {},
  users ? {},
  sources ? {},
  system ? builtins.currentSystem
}:

with pkgs.lib;

rec {
  hostNames = attrNames (
    filterAttrs (
      name: type: type == "directory"
    ) (
      builtins.readDir hostsDir
    )
  );

  hostConfig = hostName: { config, ... }: {
    _module.args = {
      inherit hosts groups;
    };
    imports = [
      (import (hostsDir + "/${hostName}/configuration.nix"))
      # urgh, yes, we still need to manually import the deploy module for now
      # at least if i want to keep my thing reusable.
      ../modules/deploy
    ] ++ commonImports;
    networking = {
      inherit hostName;
    };
    nixpkgs.pkgs =
      let pkgs = import pkgsPath {
        inherit (config.nixpkgs) config system;
      };
      in if pkgs ? nixpkgs then pkgs.nixpkgs else pkgs;
  };

  evalConfig = extraSpecial: config:
    import (pkgs.path + "/nixos/lib/eval-config.nix") {
      modules = [
        config
        (if sources ? home-manager then sources.home-manager + "/nixos" else {})
      ];
      specialArgs = { inherit modules overlays profiles users sources evalConfig; } // extraSpecial;
      inherit system;
    };

  hosts = listToAttrs (
    map (
      hostName: nameValuePair hostName (evalConfig {} (hostConfig hostName))
    ) hostNames
  );

  groupNames = unique (
    concatLists (
      mapAttrsToList (
        name: host: host.config.hexchen.deploy.groups
      ) hosts
    )
  );

  groups = listToAttrs (
    map (
      groupName: nameValuePair groupName (
        filter (
          host: elem groupName host.config.hexchen.deploy.groups
        ) (
          attrValues (
            filterAttrs (_: h: h.config.hexchen.deploy.enable) hosts
          )
        )
      )
    ) groupNames
  );
}
