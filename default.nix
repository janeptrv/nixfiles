{ system ? builtins.currentSystem }:

let
  modList = import ./lib/modules.nix;
in rec {
  # custom repositories list
  sources = import ./nix/sources.nix;

  # composable things
  modules = modList { modulesDir = ./modules; };
  profiles = modList { modulesDir = ./profiles; };
  overlays = modList { modulesDir = ./overlays; importAll = true; };
  users = modList { modulesDir = ./users; };

  # apply overlays to all pkgs
  pkgs = import ./pkgs { overlays = builtins.attrValues overlays; };
  
  # package binaries
  inherit (pkgs.nixpkgs) lib;
  # host settings
  inherit (import ./lib/hosts.nix { inherit system modules profiles overlays users sources; pkgs = pkgs; }) hosts groups;
  # deploy script
  deploy = import ./lib/deploy.nix { pkgs = pkgs.nixpkgs; inherit hosts groups; };
}
