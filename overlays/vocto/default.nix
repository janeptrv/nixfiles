self: super: {
  voctosrc = super.fetchFromGitHub {
    owner = "voc";
    repo = "voctomix";
    rev = "23b01b1860cc65ce73e2135c9f7bd9820f938e24";
    sha256 = "19p8gh45fv7a7gd9lq3va9l6d1m3p5b643wxfx18y2wp28x3xz26";
  };

  voctoPython = super.python3Packages.override {
    overrides = pself: psuper: {
      vocto = super.python3Packages.callPackage ./vocto { src = self.voctosrc; };
    };
  };
  voctocore = super.callPackage ./voctocore { src = self.voctosrc; python3Packages = self.voctoPython; };
  voctogui = super.callPackage ./voctogui { src = self.voctosrc; python3Packages = self.voctoPython; };
}
