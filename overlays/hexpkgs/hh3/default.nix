{ mkYarnPackage }:

mkYarnPackage {
  name = "hh3";
  # currently, hh3 is private. builds will fail unless you have been given access to the repo.
  src = builtins.fetchGit {
    url = "git@gitlab.com:Mstrodl/hh3.git";
    rev = "9603c499ced738b56ceb090ee7ee8dc6b468ea4a";
    ref = "master";
  };

  yarnLock = ./yarn.lock;
}
