let
  sources = import ../../nix/sources.nix;
in self: super: {
  dino = super.dino.overrideAttrs ({patches ? [], ...}: {
    patches = patches ++ [
      "${sources.qyliss-nixlib}/overlays/patches/dino/0001-add-an-option-to-enable-omemo-by-default-in-new-conv.patch"
    ];
  });

  docker = super.docker.overrideAttrs (psuper: {
    moby = psuper.moby.overrideAttrs (psuper: {
      extraPath = psuper.extraPath + ":${super.zfs}/bin";
    });
  });

  dendrite = super.callPackage ./dendrite {};

  hh3 = super.callPackage ./hh3 {};
  hh3cord = super.discord.overrideAttrs (dsuper: {
    installPhase = dsuper.installPhase + "\n" + ''
        ${self.hh3}/bin/hh3 $out/opt/Discord/Discord
      '';
  });
  hh3cord-canary = super.discord-canary.overrideAttrs (dsuper: {
    installPhase = dsuper.installPhase + "\n" + ''
        ${self.hh3}/bin/hh3 $out/opt/DiscordCanary/DiscordCanary
      '';
  });

  botamusique = super.callPackage ./botamusique { };

  binaryninja = super.callPackage ./binaryninja { };
  yggdrasil = super.callPackage ./yggdrasil {};

  quasselClient = super.quasselClient.overrideAttrs (psuper: let
    v = if psuper.version != "0.13.1" then "quassel updated, fix your shit you idiot" "0.14-rc1" else "0.14-rc1";
  in rec {
    version = v;
    src = super.fetchFromGitHub {
      owner = "quassel";
      repo = "quassel";
      rev = version;
      sha256 = "1vnn0nfqk530k861h8jr44g8lwsf828wv6n2acfza9wrcipdv4ys";
    };
    buildInputs = psuper.buildInputs ++ [ super.boost ];
    patches = [];
  });
}
