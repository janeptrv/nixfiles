{
  python3Packages, fetchFromGitHub, fetchurl, libopus, substituteAll, stdenv, ffmpeg, file, lib
}:

let
  pyradios = python3Packages.buildPythonPackage rec {
    pname = "pyradios";
    version = "0.0.22";
    src = fetchurl {
      url = "https://files.pythonhosted.org/packages/1e/1f/2fd555177390cbde507f1eee8115841d6ce712fe146eb1d1be77facb0d54/pyradios-0.0.22-py3-none-any.whl";
      sha256 = "1v2iy6pjxyg53f858qbi0rfsin3zynlry9vir8lmk7582nkg38pz";
    };
    format = "wheel";
    doCheck = false;
    propagatedBuildInputs = with python3Packages; [ appdirs requests ];
  };
  protobuf = python3Packages.buildPythonPackage rec {
    pname = "protobuf";
    version = "3.12.2";
    src = fetchurl {
      url = "https://files.pythonhosted.org/packages/ab/e7/8001b5fc971078a15f57cb56e15b699cb0c0f43b1dffaa2fae39961d80da/protobuf-3.12.2.tar.gz";
      sha256 = "1xbdxx2a9m2ixq6jqfbqf0ag4hlgl3klz2gsfsdah4kqqas8mvs9";
    };
    format = "setuptools";
    doCheck = false;
    propagatedBuildInputs = with python3Packages; [ setuptools six ];
  };
  python-magic = python3Packages.buildPythonPackage rec {
    pname = "python-magic";
    version = "0.4.22";
    src = fetchurl {
      url = "https://files.pythonhosted.org/packages/ba/7c/1d1d4bdda29bfec662b9b50951dee2dddf7747d3cbf7777f3d1c63372bd0/python_magic-0.4.22-py2.py3-none-any.whl";
      sha256 = "1bhyayjykpmrn2mp577j14kawm35nan95qyr1dwrhcwsq02fhlc5";
    };
    postInstall = ''
      substituteInPlace $out/lib/python3.8/site-packages/magic/loader.py --replace "ctypes.util.find_library('magic')" "'${file}/lib/libmagic${stdenv.hostPlatform.extensions.sharedLibrary}'"
    '';
    format = "wheel";
    doCheck = false;
    propagatedBuildInputs = [ file ];
  };
  opuslib = python3Packages.buildPythonPackage rec {
    pname = "opuslib";
    version = "3.0.1";
    src = fetchurl {
      url = "https://files.pythonhosted.org/packages/46/55/826befabb29fd3902bad6d6d7308790894c7ad4d73f051728a0c53d37cd7/opuslib-3.0.1.tar.gz";
      sha256 = "15sc0jdbdprj3v2i074nypcdppfx0hs1whzyzq6wazryn3jlbc1c";
    };
    format = "setuptools";
    doCheck = false;
    patches = [
      (substituteAll {
        src = ./opuslib-paths.patch;
        opusLibPath = "${libopus}/lib/libopus${stdenv.hostPlatform.extensions.sharedLibrary}";
      })
    ];
  };
  pymumbleNew = python3Packages.buildPythonPackage rec {
    pname = "pymumble";
    version = "1.6";
    src = fetchurl {
      url = "https://files.pythonhosted.org/packages/9f/89/08614c00cc2cd8400f0fab7abee81d36c0c3dc79418f4e722a54e502541d/pymumble-1.6.tar.gz";
      sha256 = "1szghw16s8rgfl01arx8l8pi2bka73w1xd4xs4smz81xrqmnd181";
    };
    format = "setuptools";
    doCheck = false;
    propagatedBuildInputs = [ opuslib protobuf ];
  };
in python3Packages.buildPythonApplication rec {
  pname = "botamusique";
  version = "7.2";
  format = "other";

  # ideally, we want to use this. but then the frontend wouldn't work.
  #src = fetchFromGitHub {
  #  owner = "azlux";
  #  repo = "botamusique";
  #  rev = version;
  #  sha256 = "1xn5xghrw753y3q78z3lb28jvgayx79ck22f5zb6ydnk1qigqiy5";
  #};

  src = fetchurl {
    url = "http://packages.azlux.fr/botamusique/sources-stable.tar.gz";
    sha256 = "0ilkwb1h5dgkz23p3j2a4y16p329q2y496i6vmlxmlmj2fcg3hay";
  };

  propagatedBuildInputs = with python3Packages; [
    flask
    youtube-dl
    python-magic
    pillow
    mutagen
    requests
    packaging
    pymumbleNew
    pyradios
  ];

  strictDeps = false;

  installPhase = ''
    runHook preInstall
    mkdir -p $out/bin $out/lib
    cp -r . $out/lib/botamusique
    ln -s $out/lib/botamusique/mumbleBot.py $out/bin/botamusique
    runHook postInstall
  '';

  postFixup = ''
    patchPythonScript $out/lib/botamusique/mumbleBot.py
    chmod +x $out/lib/botamusique/mumbleBot.py
    wrapProgram $out/bin/botamusique --run "cd $out/lib/botamusique" --suffix PATH : ${lib.makeBinPath [ffmpeg python3Packages.python]}
  '';
}
