{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.hexchen.bindmounts;

in {
  options.hexchen.bindmounts = mkOption {
    type = types.attrsOf types.str;
    default = {};
    example = {
      "/etc/asdf" = "/persist/asdf";
    };
  };

  config.fileSystems = mapAttrs (_: device: { inherit device; options = [ "bind" ]; }) cfg;
}
