{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.hexchen.nftables.nat;
  nats = config.networking.nat;
in {
  options.hexchen.nftables.nat = {
    enable = mkEnableOption "Wrap NAT into nftables.";
    forwardPorts = mkOption {
      type = with types; listOf (submodule {
        options = {
          ports = mkOption {
            type = types.listOf (types.either types.int (types.strMatching "[[:digit:]]+-[[:digit:]]+"));
          };
          destination = mkOption {
            type = types.str;
            example = "10.0.0.1";
          };
          proto = mkOption {
            type = types.str;
            default = "tcp";
            example = "udp";
          };
        };
      });
      default = [];
      example = [{ ports = [ 8080 "9100-9200" ]; destination = "192.168.100.2"; proto = "udp"; }];
    };
  };

  config = mkIf cfg.enable {
    networking.nat.enable = mkOverride 10 false;

    boot = {
      kernelModules = [ "nf_nat_ftp" ];
      kernel.sysctl = {
        "net.ipv4.conf.all.forwarding" = mkOverride 97 true;
        "net.ipv4.conf.default.forwarding" = mkOverride 97 true;
      };
    };

    networking.nftables = {
      enable = true;

      extraConfig = ''
        table ip nat {
          chain prerouting {
            type nat hook prerouting priority -100
            ${concatMapStringsSep "\n" (rule: "iifname ${nats.externalInterface} ${rule.proto} dport { ${concatStringsSep ", " (map (x: toString x) rule.ports)} } dnat ${rule.destination}") cfg.forwardPorts}
          }
          chain postrouting {
            type nat hook postrouting priority 100
            ${concatMapStringsSep "\n" (iface: "iifname ${replaceStrings ["+"] ["*"] iface} oifname ${nats.externalInterface} masquerade") nats.internalInterfaces}
            ${concatMapStringsSep "\n" (addr: "ip saddr ${addr} oifname ${nats.externalInterface} masquerade") nats.internalIPs}
          }
        }
      '';
    };
  };
}
