{ config, pkgs, lib, ... }:

with lib;

let
  cfg = config.hexchen.deploy;

in {
  options = {
    hexchen.deploy = {
      enable = mkOption {
        type = types.bool;
        default = true;
      };
      ssh.host = mkOption {
        type = types.str;
        default = "${config.networking.hostName}.${config.networking.domain}";
      };
      ssh.port = mkOption {
        type = types.int;
        default = head config.services.openssh.ports;
      };
      substitute = mkOption {
        type = types.bool;
        default = true;
      };
      groups = mkOption {
        type = with types; listOf str;
        default = [];
      };
    };
  };

  config = mkIf cfg.enable {
    hexchen.deploy.groups = [ "all" ];

    system.build.deployScript = pkgs.writeScript "deploy-${config.networking.hostName}" ''
      #!${pkgs.runtimeShell}
      set -xeo pipefail
      export PATH=${with pkgs; lib.makeBinPath [ coreutils openssh nix ]}
      export NIX_SSHOPTS="$NIX_SSHOPTS -p${toString cfg.ssh.port}"
      nix copy ${if cfg.substitute then "-s" else ""} --no-check-sigs --to ssh://${cfg.ssh.host} ${config.system.build.toplevel}
      if [ "$1" == "switch" ] || [ "$1" == "boot" ]; then
        ssh $NIX_SSHOPTS ${cfg.ssh.host} "sudo nix-env -p /nix/var/nix/profiles/system -i ${config.system.build.toplevel}"
      fi
      ssh $NIX_SSHOPTS ${cfg.ssh.host} "sudo ${config.system.build.toplevel}/bin/switch-to-configuration $1"
    '';
  };
}
