{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.services.botamusique;

  configFile = pkgs.writeText "botamusique.ini" (generators.toINI {} cfg.config);
in {
  options.services.botamusique = {
    enable = mkEnableOption "music bot for mumble";
    config = mkOption {
      type = types.attrs;
      default = {};
    };
  };

  config = mkIf cfg.enable {
    users.users.botamusique.isSystemUser = true;

    systemd.services.botamusique = {
      after = [ "network.target" ];
      wantedBy = [ "multi-user.target" ];
      serviceConfig = rec {
        User = "botamusique";
        StateDirectory = "botamusique";
        CacheDirectory = "botamusique";
        ExecStart = "${pkgs.botamusique}/bin/botamusique --config ${configFile}";
        Environment = "HOME=/var/lib/botamusique";
        Restart = "on-failure";

        NoNewPrivileges = true;
        AmbientCapabilities = "";
        CapabilityBoundingSet = "";
        DeviceAllow = "";
        LockPersonality = true;
        PrivateTmp = true;
        PrivateDevices = true;
        PrivateUsers = true;
        ProtectClock = true;
        ProtectControlGroups = true;
        ProtectHostname = true;
        ProtectKernelLogs = true;
        ProtectKernelModules = true;
        ProtectKernelTunables = true;
        RemoveIPC = true;
        RestrictNamespaces = true;
        RestrictAddressFamilies = [ "AF_INET" "AF_INET6" ];
        RestrictRealtime = true;
        RestrictSUIDSGID = true;
        SystemCallArchitectures = "native";
        SystemCallErrorNumber = "EPERM";
        SystemCallFilter = [
          "@system-service"
          "~@chown" "~@cpu-emulation" "~@debug" "~@keyring" "~@memlock" "~@module"
          "~@obsolete" "~@privileged" "~@setuid"
        ];
      };
    };
  };
}
