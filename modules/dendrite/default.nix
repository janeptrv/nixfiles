{ config, lib, pkgs, ... }:

let
  cfg = config.hexchen.dendrite;
in {
  options.hexchen.dendrite = {
    enable = lib.mkEnableOption "dendrite matrix homeserver";

    config = lib.mkOption {
      type = lib.types.attrs;
    };

    dataDir = lib.mkOption {
      type = lib.types.str;
      default = "/var/lib/dendrite";
    };
  };

  config = lib.mkIf cfg.enable {
    systemd.services.dendrite = {
      after = [ "network.target" ];
      wantedBy = [ "multi-user.target" ];

      preStart = ''
        if [ ! -f ${cfg.dataDir}/matrix_key.pem ]; then
          ${pkgs.dendrite}/bin/generate-keys --private-key ${cfg.dataDir}/matrix_key.pem
        fi
        ${pkgs.remarshal}/bin/json2yaml -i ${pkgs.writeText "config.json" (builtins.toJSON cfg.config)} -o ${cfg.dataDir}/config.yaml
      '';
      serviceConfig = {
        DynamicUser = true;
        StateDirectory = "dendrite";
        WorkingDirectory = cfg.dataDir;
        ExecStart = "${pkgs.dendrite}/bin/dendrite-monolith-server --config ${cfg.dataDir}/config.yaml";
        Restart = "on-failure";
      };
    };
  };
}
