{
  bird = ./bird;
  wireguard = ./wireguard;
  yggdrasil = ./yggdrasil;
  nftables = ./nftables;
  policyrouting = ./policyrouting;
}
