{ config, lib, pkgs, hosts, ... }:

with lib;

let
  cfg = config.network.wireguard;
  hcfg = _: h: h.config.network.wireguard;
  netHostsSelf = mapAttrs hcfg (filterAttrs (_: x: x.config.network.wireguard.enable or false) hosts);
  netHosts = filterAttrs (_: x: x.pubkey != cfg.pubkey) netHostsSelf;
in {
  options.network.wireguard = {
    enable = mkEnableOption "semi-automatic wireguard mesh";
    magicNumber = mkOption { type = types.ints.u8; };
    prefix = mkOption { type = types.str; default = "172.23.1"; };
    keyPath = mkOption { type = types.str; default = "/etc/wireguard/mesh"; };
    pubkey = mkOption { type = types.str; };
    publicAddress4 = mkOption { type = types.nullOr types.str; default = null; };
    publicAddress6 = mkOption { type = types.nullOr types.str; default = null; };
    fwmark = mkOption { type = types.nullOr types.ints.u16; default = null; };
    mtu = mkOption { type = types.ints.u16; default = 1500; };
  };

  config = mkIf cfg.enable {
    networking.wireguard.interfaces = mapAttrs' (hname: hconf:
      let
        magicPort = 51820 + hconf.magicNumber + cfg.magicNumber;
        iname = "wgmesh-${substring 0 8 hname}";
      in nameValuePair iname {
        allowedIPsAsRoutes = false;
        privateKeyFile = cfg.keyPath;
        ips = [
          "${cfg.prefix}.${toString cfg.magicNumber}/24"
          "fe80::${toString cfg.magicNumber}/64"
        ];
        listenPort = magicPort;
        peers = [ {
          publicKey = hconf.pubkey;
          allowedIPs = [ "0.0.0.0/0" "::/0" ];
          endpoint = with hconf; mkIf (publicAddress4 != null || publicAddress6 != null) (
            if (publicAddress4 != null)
            then "${publicAddress4}:${toString magicPort}"
            else "[${publicAddress6}]:${toString magicPort}"
          );
          persistentKeepalive = with hconf; mkIf (publicAddress4 == null && publicAddress6 == null) 25;
        } ];
        postSetup = ''
          ip route add ${cfg.prefix}.${toString hconf.magicNumber}/32 dev ${iname}
          ${optionalString (cfg.fwmark != null) "wg set ${iname} fwmark ${toString cfg.fwmark}"}
          ip link set ${iname} mtu ${toString cfg.mtu}
        '';
      }
    ) (filterAttrs (_:x: !(x.publicAddress4 == null && x.publicAddress6 == null && cfg.publicAddress4 == null && cfg.publicAddress6 == null)) netHosts);
    networking.firewall.allowedUDPPorts =
      mapAttrsToList (_: hconf: 51820 + hconf.magicNumber + cfg.magicNumber) netHosts;
  };
}
